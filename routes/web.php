<?php

//Route::get('/home', function () {
//    return redirect('');
//});

//Route::get('/join','NewsController@store');

Route::post('/news/show','NewsController@store');
Route::get('/news/{news}','NewsController@show');
Route::get('/', 'NewsController@home');


Route::post('/events/show','EventsController@store');
Route::get('/events/{events}','EventsController@show');

Route::get('/', 'EventsController@home');
//Route::get('/', 'NewsController@home');

Route::get('/acmembers/create', 'AcmembersController@create');
Route::post('/acmembers', 'AcmembersController@store');
Route::get('/acmembers','AcmembersController@show');


Route::get('/communities/create', 'CommunitiesController@create')->name('communities.create');
Route::post('/communities','CommunitiesController@store')->name('communities.create');
Route::get('/communities', 'CommunitiesController@index')->name('communities');
//Route::get('/communities', 'CommunitiesController@search');




Route::get('/contact', 'ContactController@create');
Route::post('/contact', 'ContactController@store');
Route::get('/admin/message', 'ContactController@show');

Route::get('/admin/acmembers', 'AcmembersController@admin');
//Route::get('/admin/search/acmembers', 'AcmembersController@search');


Route::post('/news/news/{id}', 'NewsController@destroy')->name('del');
Route::post('/events/events/{id}', 'EventsController@destroy')->name('delet');



Route::get('/register', 'RegistrationController@create')->middleware('auth');
Route::post('/register', 'RegistrationController@store');


//Auth::routes();
Route::get('/login', 'Auth\LoginController@index')->name('login');
Route::post('/login','Auth\LoginController@login')->name('login');

Route::get('/admin/home', function () {return view('admin.home');});


Route::get('/logout', 'SessionsController@destroy');