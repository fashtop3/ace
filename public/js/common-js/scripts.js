
(function ($) {

    "use strict";
	
	
			
	// Preloader
	
	$(window).on('load', function() { 
		$('#status').fadeOut();  
		$('#preloader').delay(350).fadeOut('slow');   
		$('body').delay(350).css({'overflow-y':'visible'});
    });
	
	
	// Add slideDown animation to Bootstrap dropdown when expanding.
	
	$('.dropdown').on('show.bs.dropdown', function() {
		$(this).find('.dropdown-menu').first().stop(true, true).fadeIn(100);
	});

	// Add slideUp animation to Bootstrap dropdown when collapsing.
	  
	$('.dropdown').on('hide.bs.dropdown', function() {
		$(this).find('.dropdown-menu').first().stop(true, true).fadeOut(100);
	});
		
	$('.dropdown').on('mouseenter', function() {
		$(this).find('.dropdown-menu').first().stop(true, true).fadeIn(200);
	});

	// Add slideUp animation to Bootstrap dropdown when collapsing.
	  
	$('.dropdown').on('mouseleave', function() {
		$(this).find('.dropdown-menu').first().stop(true, true).fadeOut(200);
	});
	
	
	$('.osthir-carousel').each(function () {
		var carousel = $(this),
			loop = carousel.data('loop'),
			items = carousel.data('items'),
			margin = carousel.data('margin'),
			autoplay = carousel.data('autoplay'),
			autoplayHoverPause = carousel.data('autoplay-hover-pause'),
			autoplayTimeout = carousel.data('autoplay-timeout'),
			smartSpeed = carousel.data('smart-speed'),
			dots = carousel.data('dots'),
			nav = carousel.data('nav'),
			
			Xsmall = carousel.data('x-small'),
			Xmedium = carousel.data('x-medium'),
			Small = carousel.data('small'),
			Medium = carousel.data('medium'),
			Large = carousel.data('large');
			
		carousel.owlCarousel({
			loop: (loop ? true : false),
			items: (items ? items : 4),
            margin: (margin ? margin : 0),
            autoplay: (autoplay ? true : false),
            autoplayHoverPause: (autoplayHoverPause ? true : false),
            autoplayTimeout: (autoplayTimeout ? autoplayTimeout : 1000),
            smartSpeed: (smartSpeed ? smartSpeed : 250),
			dots: (dots ? true : false),
            nav: (nav ? true : false),
			navText: ["<i class='fa fa-long-arrow-left' aria-hidden='true'></i>", "<i class='fa fa-long-arrow-right' aria-hidden='true'></i>"],
			responsiveClass: true,
			responsive: {
                0: { items: ( Xsmall ? Xsmall : 1) },
                550: { items: ( Xmedium ? Xmedium : 2) },
                768: { items: ( Small ? Small : 3) },
                992: { items: ( Medium ? Medium : 5) },
                1199: { items: ( Large ? Large : 6) }
            }
		});
		
	});
	
  
	// Icon change of Notice Section 
	
	$('.collapse').on('shown.bs.collapse', function (e) {
		$(this).parent().find('.fa-plus').removeClass('fa-plus').addClass('fa-minus').addClass('rotate-icon');
	}).on('hidden.bs.collapse', function(e){
		$(this).parent().find('.fa-minus').removeClass('fa-minus').addClass('fa-plus').removeClass('rotate-icon');
	});
	
	
	// Carousel Images as background of the slider

	$('.full-height .item> img').each(function(){
		var srcImg = $(this).attr('src');
		$(this).parent().css({
			'background-image':'url('+srcImg+')'
			});
		$(this).remove();
	});
	
	
	var $myCarousel = $('.carousel'),
        $firstAnimatingElems = $myCarousel.find('.item:first').find("[data-animation ^= 'animated']");
        
	
    //Animate captions in first slide on page load
	
    doAnimations($firstAnimatingElems);
    
    
    //Other slides to be animated on carousel slide event 
    
	$myCarousel.on('slide.bs.carousel', function (e) {
        var $animatingElems = $(e.relatedTarget).find("[data-animation ^= 'animated']");
        doAnimations($animatingElems);
    }); 
	
	$('#main-carousel').carousel({
        interval:10000,
		pause: "hover"
    });
	
	
	// Start Count of the counter with scroll 
	
	var top_of_window = $(window).scrollTop();
	var bottom_of_window = $(window).scrollTop() + $(window).height();
	
	

	
	// Make main menu Fixed with Scroll 
	
	var mainMenuId = isExists('#main-menu') ? $('#main-menu') : null;
	var mainMenuTop = isNull(mainMenuId) ? 0 : mainMenuId.offset().top;
	var mainMenuBottom = isNull(mainMenuId) ? 0 : mainMenuId.offset().top + mainMenuId.outerHeight();
	mainMenuBottom = 35;
	
	if( (top_of_window > mainMenuBottom) ){
		$('#main-menu').addClass('fixed-manu');
		
		// Change Image Based on Background
		if($('.navbar-header .main-logo img').attr('data-img-change')  == 'enable'){
			$('.navbar-header .main-logo img').attr('src', '/images/acm.png');
		}
	}
	else{
		$('#main-menu').removeClass('fixed-manu');
		
		// Change Image Based on Background
		if($('.navbar-header .main-logo img').attr('data-img-chage')  == 'enable'){
			$('.navbar-header .main-logo img').attr('src', '/images/acm.png');
		}
	}
	
	var moveToTopBtn = isExists('.move-to-top') ? $('.move-to-top') : null;

	
	// Make all page load animated element's parent overflow hidden
	
	if($(window).width() > 767){
		$('.enable-load-effect [data-load-effect]').css('opacity', '0');
	}
	
	// Animate elements with Scroll effect
	
	$('.enable-load-effect').each(function(){
		var animationDelay = 0;
		var top_of_object_previous;
		$(this).find('[data-load-effect]').each(function(){
			
			var top_of_object = $(this).offset().top;
			
			// Check if the elements are in same line or new line
			if(top_of_object > (top_of_object_previous + 200)){ 
				animationDelay = 0; 
			}
			top_of_object_previous = top_of_object;
			
			if( top_of_window > (top_of_object - ($(window).height()/1.1)) ){
				var loadEffect = $(this).attr('data-load-effect');
				
				$(this).addClass(loadEffect).css('animation-delay', animationDelay+'s');
				animationDelay += .1;
			}
		});
	});

	
	// Course without sidebar horizontal or vertical list button click event
	
	$('#horizintal-list').click(function(){
		$('.course').parent().removeClass().addClass('col-sm-12');
		$('.course').removeClass('course').addClass('horizontal-course');
		return false;
	});
	$('#vertical-list').click(function(){
		$('.horizontal-course').parent().removeClass().addClass('col-sm-6 col-md-4');
		$('.horizontal-course').removeClass('horizontal-course').addClass('course');
		return false;
	});
	
	
	// Course with sidebar horizontal or vertical list button click event
	
	$('#horizintal-list-sidebar').click(function(){
		$('.course').parent().removeClass().addClass('col-sm-12');
		$('.course').removeClass('course').addClass('horizontal-course');
		return false;
	});
	$('#vertical-list-sidebar').click(function(){
		$('.horizontal-course').parent().removeClass().addClass('col-sm-12 col-md-6');
		$('.horizontal-course').removeClass('horizontal-course').addClass('course');
		return false;
	});
	
	var counterVar = 0;
	
	// Page Scroll Event
	
	$(window).scroll( function(){
	
	
	// Counter starts on visible 
	
		var counterId = isExists('.counter') ? $('.counter') : null;
		var oTop = (counterId == null) ? 0 : counterId.offset().top - window.innerHeight;
		if (counterVar == 0 && $(window).scrollTop() > oTop) {
			$('.counter-value').each(function() {
				var $this = $(this),
					countTo = $this.attr('data-count');
				$({
					countNum: $this.text()
				}).animate({
					countNum: countTo
				},
				{
					duration: 2000,
					easing: 'swing',
					step: function() {
						$this.text(Math.floor(this.countNum));
					},
					complete: function() {
						$this.text(this.countNum);
					}

				});
			});
			counterVar = 1;
		}
		
		top_of_window = $(window).scrollTop();
		bottom_of_window = $(window).scrollTop() + $(window).height();
		
		
		// Animate elements with Scroll effect
		
		$('.enable-load-effect').each(function(){
			var animationDelay = 0;
			var top_of_object_previous;
			$(this).find('[data-load-effect]').each(function(){
				
				var top_of_object = $(this).offset().top;
				
				// Check if the elements are in same line or new line
				if(top_of_object > (top_of_object_previous + 200)){ 
					animationDelay = 0; 
				}
				top_of_object_previous = top_of_object;
				
				if( top_of_window > (top_of_object - ($(window).height()/1.1)) ){
					var loadEffect = $(this).attr('data-load-effect');
					
					$(this).addClass(loadEffect).css('animation-delay', animationDelay+'s');
					animationDelay += .1;
				}
			});
		});

		
		// Scroll to top button visible in window height * 1.5 
		
		if(top_of_window > ($(window).height() * 1.5) ){ 
			if(moveToTopBtn != null){ $(moveToTopBtn).addClass('visible-move-top-btn'); }
		}else{ $(moveToTopBtn).removeClass('visible-move-top-btn');}
		
		
	
		
		// Make main menu Fixed with Scroll 
		
		mainMenuTop = isNull(mainMenuId) ? 0 : mainMenuId.offset().top;
		mainMenuBottom = isNull(mainMenuId) ? 0 : mainMenuId.offset().top + mainMenuId.outerHeight();
		mainMenuBottom = 35;
		
		
		if( (top_of_window > mainMenuBottom) ){
			$('#main-menu').addClass('fixed-manu');
			
			// Change Image Based on Background
			if($('.navbar-header .main-logo img').attr('data-img-change') == 'enable'){
				$('.navbar-header .main-logo img').attr('src', '/images/acm.png');
			}
		}
		else{
			$('#main-menu').removeClass('fixed-manu');
			
			// Change Image Based on Background
			if($('.navbar-header .main-logo img').attr('data-img-change') == 'enable'){
				$('.navbar-header .main-logo img').attr('src', '/images/acm.png');
			}
		}
		
	});
	
	
	// Search button click event
	
	$('.search-btn').click(function(){
		$('.search-area').toggleClass('search-area-anim');
		return false;
	});
	$('.search-area .close-btn').click(function(){
		$('.search-area').toggleClass('search-area-anim');
		return false;
	});
	
	
	// Scroling animation for the same page
	
	scrollSpy(('a.scroll-animation-enable'), 1000);
	
	
})(jQuery);


function scrollSpy(elem, duration){
	$(elem).click(function(){
		if(location.pathname.replace(/^\//, '')==
			this.pathname.replace(/^\//, '') && location.hostname == this.hostname){
			var target = $(this.hash);
			target = target.length ? target : $('[name='+ this.hash.Slice(1) +']');
			if(target.length){
				$('html,body').animate({
					scrollTop: target.offset().top,	
				}, duration);
				return false;
			}
		}
		
	});
	
}

function isNull(elems){
	return (elems === null) ? true : false;
}

function isExists(elems){
	if ($(elems).length > 0) { 
		return true;
	}
	return false;
}

function doAnimations( elems ) {
	
    //Cache the animationend event in a variable
	
    var animEndEv = 'webkitAnimationEnd animationend';
    
    elems.each(function () {
        var $this = $(this),
            $animationType = $this.data('animation');
        $this.addClass($animationType).one(animEndEv, function () {
            $this.removeClass($animationType);
        });
	});
}


			// $('#join').submit( function (e) {
            //
            //
            // var fname = $('#fname').val();
            // var lname = $('#lname').val();
            // var email = $('#email').val();
            // var phone = $('#phone').val();
            // var dept = $('#dept').val();
            // var level = $('#level').val();
            // var sgroup = $('#sgroup').val();
            //
            // $.post(
             //    "join.php",
             //    { fname:fname, lname:lname, email:email, phone:phone, dept:dept, level: level, sgroup:sgroup },
             //    function(data,status){
             //        alert("Data: " + data+ "\nStatus: " + status);
             //    }
            // );
            //
            // e.preventDefault();
            //
            //
			// });

		$('#join').submit(function (e) {

			var data = $(this).serialize();

			$.ajax({
				type: 'POST',
				url: 'join.php',
				data: data,
				success : function (data) {
					alert( "You are now a member" );

				}

			});
			e.preventDefault();

		});


	$('#signup').submit(function (e) {

		var data = $(this).serialize();

			$.ajax({
				type: 'POST',
				url: 'signup.php',
				data: data,
				success : function (data) {
					alert( "Data: " + data );

                }

		});

    });

	$('#login').submit(function (e) {

		var data = $(this).serialize();

		$.ajax({
			type: 'POST',
			url: 'login.php',
			data: data,
			success : function (data) {
				// alert( "form submitted" );
				window.location.href = "/lyceum/adm.php";
			}
		});
	});

    $('#news').submit(function (e) {
        // var data = $(this).serialize();

        $.ajax({
            type: 'POST',
            url: 'adm.php',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            success : function (data) {
                alert( data + "yahoo");
                // window.location.href = "/lyceum/adhome.php";
            }
        });

	});











        // var xhr = new XMLHttpRequest();
        // xhr.onload = function() {
        //     console.log("Upload complete.");
        // };
        //
        // xhr.onreadystatechange = function() {
        //     if (xhr.readyState == 4 && xhr.status == 200) {
        //
        //
        //
        //     }
        // };
        // xhr.open("post", "adm.php", true);
        // xhr.send(form);



      /*  var data = $(this).serialize();

        $.ajax({
            type: 'POST',
            url: 'adm.php',
            data: data,
            success : function (data) {
                alert( data );
                // window.location.href = "/lyceum/adhome.php";
            }
        }); */


	$("#uploadimage").submit(function(e) {
		e.preventDefault();
		$("#message").empty();
		$('#loading').show();
		$.ajax({
			url: "blank.php", // Url to which the request is send
			type: "POST",             // Type of request to be send, called as method
			data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,        // To send DOMDocument or non processed data file it is set to false
			success: function(data)   // A function to be called if request succeeds
			{
				$('#loading').hide();
				$("#message").html(data);
			}
		});
	});



$("#news").submit(function(e) {
    e.preventDefault();
    // $("#message").empty();
    // $('#loading').show();
    $.ajax({
        url: "adm.php", // Url to which the request is send
        type: "POST",             // Type of request to be send, called as method
        data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
        contentType: false,       // The content type used when sending data to the server.
        cache: false,             // To unable request pages to be cached
        processData:false,        // To send DOMDocument or non processed data file it is set to false
        success: function(data)   // A function to be called if request succeeds
        {
            $('#loading').hide();
            $("#message").html(data);
        }
    });
});

$('#event').submit(function (e) {

    var data = $(this).serialize();

    $.ajax({
        type: 'POST',
        url: 'adm.php',
        data: data,
        success : function (data) {
            // alert( "form submitted" );
            // window.location.href = "/lyceum/adhome.php";
        }
    });
});

$( function() {
    $( "#datepicker" ).datepicker();
} );



// Function to preview image after validation
// $(function() {
//     $("#file").change(function() {
//         $("#message").empty(); // To remove the previous error message
//         var file = this.files[0];
//         var imagefile = file.type;
//         var match= ["image/jpeg","image/png","image/jpg"];
//         if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
//         {
//             $('#previewing').attr('src','noimage.png');
//             $("#message").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
//             return false;
//         }
//         else
//         {
//             var reader = new FileReader();
//             reader.onload = imageIsLoaded;
//             reader.readAsDataURL(this.files[0]);
//         }
//     });
// });
// function imageIsLoaded(e) {
//     $("#file").css("color","green");
//     $('#image_preview').css("display", "block");
//     $('#previewing').attr('src', e.target.result);
//     $('#previewing').attr('width', '250px');
//     $('#previewing').attr('height', '230px');
// };
