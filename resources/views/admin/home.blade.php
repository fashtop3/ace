@extends('layouts.admaster')

@section('content')


    <div class="carousel-inner full-height">

        <div class="item active">
            <div class="layer black-layer"></div>
            <img src="images/contactus.jpg" alt="Main Slider">

            <div class="carousel-writing center-writing">
                <h1 class="carousel-heading title">LOGIN</h1>
                <!--					<h6 class="desc"><a href="#">HOME <i class="fa fa-angle-right"></i></a> CONTACT</h6>-->
            </div><!-- carousel-writing -->

        </div><!-- item -->

    </div><!-- carousel-inner -->
    </section><!-- carousel -->
<h1> Hey, welcome to the admins page; {{Auth::user()->name}}</h1>

@endsection