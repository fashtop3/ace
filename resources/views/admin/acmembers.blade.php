@extends('layouts.admaster')

@section('content')

    <section class="blog-area" id="featured">
        <div class="container">
            <div class="row">

                <div class="col-sm-12 col-md-12">

                    <div class="blog-post">


                        <table class="table table-hove  rtable">
                            <thead>
                            <tr>
                                <th>S/N</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Department</td>
                                <th>Level</th>
                                <th>Phone Number</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php $count = 1 ?>
                            @foreach($acmembers as $acmember )
                            <tr>
                                <td> {{$count++}}</td>
                                <td>{{$acmember->name}}</td>
                                <td>{{$acmember->email}}</td>
                                <td>{{$acmember->dept}}</td>
                                <td>{{$acmember->level}}</td>
                                <td>{{$acmember->phone}}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <!--<ul class="blog-post-info">-->


                    </div><!-- blog-post-->
                </div><!-- col-md-8 -->

            </div><!-- row -->
        </div><!-- container -->
    </section><!-- blog-area -->



@endsection