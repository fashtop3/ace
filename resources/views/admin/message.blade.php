@extends('layouts.admaster')

@section('content')


    <section class="blog-area" id="featured">
        <div class="container">
            <div class="row">

                <div class="col-sm-8 col-md-9 col-sm-offset-1">

                    <div class="blog-post">


                        <div class="blog-post">


                            <table class="table table-hove  rtable">
                                <thead>
                                <tr>
                                    <th>S/N</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Department</td>
                                    <th>Delivered</th>
                                    <th>Show message</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php $count = 1 ?>
                                @foreach($messages as $message )
                                    <tr>
                                        <td> {{$count++}}</td>
                                        <td> {{$message->name}}</td>
                                        <td> {{$message->email}}</td>
                                        <td> {{$message->department}}</td>
                                        <td> {{ $message->created_at->diffForHumans()}}</td>
                                        <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                                View Message
                                            </button>

                                            <!-- Modal -->
                                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">{{$message->subject}}</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            {{$message->message}}
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>



                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>




                    </div><!-- blog-post-->
                </div><!-- col-md-8 -->


            </div><!-- row -->
        </div><!-- container -->
    </section><!-- blog-area -->

@endsection


