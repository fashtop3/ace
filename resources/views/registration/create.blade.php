@extends('layouts.admaster')

@section('content')




    <section class="contact-area section" id="contact">
        <div class="container">
            <div class="row">



                <div class="col-sm-8 col-sm-offset-2">
                    <div class="contact-form-area">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif


                        <form method="post" action="/register">
                                {{csrf_field()}}

                                <div class="form-group">
                                    <label for="name">Name:</label>

                                    <input type="text" class="form-control" id="name" name="name">
                                </div>

                                <div class="form-group">
                                    <label for="email">Email:</label>

                                    <input type="email" class="form-control" id="email" name="email">
                                </div>

                                <div class="form-group">
                                    <label for="password">Password:</label>

                                    <input type="password" class="form-control" id="password" name="password">
                                </div>

                                <div class="form-group">
                                    <label for="password_confirmation">Password Confirmation:</label>

                                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Register</button>
                                 </div>

                            @include('layouts.errors')
                        </form>

                            <br>
                            <br>
                            <br>

                    </div><!-- contact-form-area -->
                </div><!-- col-sm-7 -->
            </div>

        </div><!-- row -->
        </div><!-- container -->
    </section><!-- contact-area -->


@endsection