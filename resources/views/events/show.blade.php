@extends('layouts.admaster')

@section('content')


    <section class="blog-area" id="featured">
        <div class="container">
            <div class="row">

                <div class="col-sm-12 col-md-9">

                    <div class="blog-post">


                        <div class="leave-reply-area">
                            <h4 class="title">Submit an event</h4>

                                <form method="post" action="/events/show" >
                                {{csrf_field()}}

                                <div class="form-group">
                                    <div class="form-group">
                                        <label for="Title">Title</label>
                                        <input type="text" class="form-control" placeholder="Title" id="title" name="title" >
                                    </div>

                                    <div class="form-group">
                                        <label for="body">Body</label>
                                        <textarea id="body" name="body" placeholder="Brief description of the event" maxlength="254" class="form-control"></textarea>
                                    </div>

                                    {{--<div class="form-group">--}}
                                        {{--<label for="body">Date</label>--}}
                                        {{--<input id="datepicker" name="date" placeholder="e.g  23" type="text" class="form-control">--}}
                                    {{--</div>--}}

                                    <div class="form-group">
                                        <label for="body">Date</label>
                                        <input id="date" name="date" placeholder="e.g  23"class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label for="body">Month</label>
                                        <input id="month" placeholder="e.g May " name="month" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label for="body">Location</label>
                                        <input id="location" name="location" class="form-control">
                                    </div>


                                    <div class="form-group">
                                        <label for="body">Duration</label>
                                        <input id="duration" placeholder=" e.g 9 AM - 11.30 AM" name="duration" class="form-control">
                                    </div>


                                    <div class="form-group col-sm-12" >
                                        <button class="btn-submit" type="submit" id="form-submit"> Submit Event</button>
                                    </div><!-- form-group -->
                                    <br>
                                    <br>

                                </div>
                            </form>
                        </div><!-- leave-reply-area -->

                    </div><!-- blog-post-->
                </div><!-- col-md-8 -->

                <div class="col-md-3">

                    <div class="popular-posts">
                        <h4 class="title">All events</h4>
                        @foreach($events as $event )

                            <div class="popular-post">
                                <div class="desc">
                                    <h6 class="post-name"><a href="#">{{$event->title}}</a></h6>
                                    <p class="date">{{ $event->created_at->diffForHumans() }} </p>
                                </div>

                                <form action="{{ route('delet', [$event->id]) }}" method="post">
                                    {{ csrf_field() }}
                                    <button class="btn btn-sm btn-danger btn-submit" type="submit" id="form-submit"> Delete</button>
                                </form>

                                {{--<a href="#"><button class="dbtn btn-submit" type="submit" id="form-submit"> Delete</button></a>--}}


                            </div><!-- popular-post -->
                        @endforeach



                    </div><!-- popular-posts -->


                </div><!-- col-md-3 -->

            </div><!-- row -->
        </div><!-- container -->
    </section><!-- blog-area -->

    @endsection




    {{--<ul></ul>--}}
{{--@foreach($events as $event )--}}
    {{--<li> {{$event->title}}</li>--}}
    {{--<li> {{$event->body}}</li>--}}

    {{--<img src="{{asset($event->picture)}}" style="width:50px; height: 50px">--}}

{{--@endforeach--}}

{{--</body>--}}
{{--</html>--}}