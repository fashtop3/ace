@extends('layouts.admaster')

@section('content')



    <section class="blog-area" id="featured">
        <div class="container">

            <div class="row">
                <div class="col-sm-4">
                    <div class="blog-post">
                    <form >
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="text" placeholder="Search Communities" class="form-control" id="search_field" name="search_field">
                        </div>

                        {{--<div class="form-group col-sm-12">--}}
                            <button class="btn-submit" name="action" value="join" type="submit" id="form-join">
                               Search</button>
                        {{--</div>--}}

                    </form>
                    </div>
                </div>


            </div>
            <div class="row">

                <div class="col-sm-12 col-md-12">

                    <div class="blog-post">


                        <table class="table table-hove  rtable">
                            <thead>
                            <tr>
                                <th>S/N</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Department</td>
                                <th>Level</th>
                                <th>Phone Number</th>
                                <th>Special Interest Group</th>

                            </tr>
                            </thead>
                            <tbody>
                           <?php $count = 1 ?>
                            @foreach($communities as $community )
                                <tr>
                                    <td> {{$count++}}</td>
                                    <td>{{$community->name}}</td>
                                    <td>{{$community->email}}</td>
                                    <td>{{$community->dept}}</td>
                                    <td>{{$community->level}}</td>
                                    <td>{{$community->phone}}</td>
                                    <td>{{$community->sig}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <!--<ul class="blog-post-info">-->


                    </div><!-- blog-post-->
                </div><!-- col-md-8 -->

            </div><!-- row -->
        </div><!-- container -->
    </section><!-- blog-area -->



@endsection