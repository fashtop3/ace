@extends('layouts.master')

@section('content')


    <div class="carousel-inner full-height">

        <div class="item active">
            <div class="layer black-layer"></div>
            <img src="/public/images/jn.jpg" alt="Main Slider">

            <div class="carousel-writing center-writing">
                <h1 class="carousel-heading title">JOIN A COMMUNITY</h1>
                <!--                <h6 class="desc"><a href="#">HOME <i class="fa fa-angle-right"></i></a> CONTACT</h6>-->
            </div><!-- carousel-writing -->

        </div><!-- item -->

    </div><!-- carousel-inner -->
    </section><!-- carousel -->

    <section class="contact-area section" id="contact">
        <div class="container">
            <div class="row">

                <div class="col-sm-12">
                    <div class="heading">
                        <h3></h3>WELCOME</h3>
                        <h6 class="heading-desc">WE are pleased to have you join us</h6>
                    </div><!-- heading -->
                </div><!-- col-sm-12 -->



                <div class="col-sm-6 col-sm-offset-3">
                    <div class="contact-form-area">

                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                            @include('layouts.errors')

                            <form method="post" action="/communities" >
                                {{csrf_field()}}


                                    <div class="form-group">

                                        <input type="text" placeholder="Name" class="form-control" id="name" name="name" >
                                    </div>


                                    <div class="form-group">

                                        <input type="text" placeholder="Email" class="form-control" id="email" name="email" >
                                    </div>



                                    <div class="form-group">

                                        <input type="text" placeholder="Level" class="form-control" id="level" name="level" >
                                    </div>



                                    <div class="form-group">
                                        <input type="text" placeholder="Department" class="form-control" id="dept" name="dept" >
                                    </div>


                                    <div class="form-group">

                                        <input type="text" placeholder="Phone Number" class="form-control" id="phone" name="phone" >
                                    </div>

                                <div class="form-group">
                                    <select  class="form-control" id="sig" name="sig">
                                        <option value="microsoft">Microsoft</option>
                                        <option value="intel" >Intel</option>
                                        <option value="google">Google</option>
                                    </select>
                                </div>

                                <div class="form-group col-sm-12">
                                    <button class="btn-submit" name="action" value="join" type="submit" id="form-join">
                                        <i class="fa fa-paper-plane"></i>Join Community</button>
                                </div>



                            </form>


                    </div><!-- contact-form-area -->
                </div><!-- col-sm-7 -->

            </div><!-- row -->
        </div><!-- container -->
    </section><!-- contact-area -->



@endsection