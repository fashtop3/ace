@extends('layouts.master')

@section('content')

    <div class="carousel-inner full-height">

        <div class="item active">
            <div class="layer black-layer"></div>
            <img src="images/contactus.jpg" alt="Main Slider">

            <div class="carousel-writing center-writing">
                <h1 class="carousel-heading title">CONTACT</h1>
                <!--					<h6 class="desc"><a href="#">HOME <i class="fa fa-angle-right"></i></a> CONTACT</h6>-->
            </div><!-- carousel-writing -->

        </div><!-- item -->

    </div><!-- carousel-inner -->
    </section><!-- carousel -->

    <section class="contact-area section" id="contact">
        <div class="container">
            <div class="row">

                <div class="col-sm-12">
                    <div class="heading">
                        <h3>GET IN TOUCH WITH US</h3>
                        <h6 class="heading-desc">DOU YOU HAVE ANY QUESTION?</h6>
                    </div><!-- heading -->
                </div><!-- col-sm-12 -->

                <div class="col-sm-4">
                    <div class="details">
                        <ul>
                            <li><i class="fa fa-map-marker"></i>
                                <span>Federal University of Techology, Akure. </span></li>
                            <li><i class="fa fa-envelope"></i>
                                <span><a href="mailto:futaacm@gmail.com" >futaacm@gmail.com</a></span>
                            <li><i class="fa fa-phone"></i>
                                <span>08134042532</span>
                                <span>09030283411</span>
                        </ul>
                    </div><!-- details -->
                </div><!-- col-sm-5 -->

                <div class="col-sm-8">
                    <div class="contact-form-area">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form method="post" action="/contact">
                            {{csrf_field()}}
                            <div class="row">

                                <div class="form-group col-sm-6" >
                                    <input type="text" aria-required="true" id="name" name="name" class="form-control"
                                           placeholder="Enter your Name" aria-invalid="true" required >
                                </div><!-- col-sm-6 -->
                                <div class="form-group col-sm-6" >
                                    <input type="email" aria-required="true" id="email" name="email" class="form-control"
                                           placeholder="Enter your Email" aria-invalid="true" required>
                                </div><!-- col-sm-6 -->

                                <div class="form-group col-sm-6" >
                                    <input type="text" aria-required="true" id="department" name="department" class="form-control"
                                           placeholder="Department" aria-invalid="true" required>
                                </div><!-- col-sm-6 -->

                                <div class="form-group col-sm-6" >
                                    <input type="text" aria-required="true" id="subject" maxlength="254" name="subject" class="form-control"
                                           placeholder="Enter your Subject" aria-invalid="true" required>
                                </div><!-- col-sm-6 -->

                                <div class="form-group col-sm-12" >
									<textarea name="message" id="message" rows="5" maxlength="254" class="messge-control form-control"
                                              placeholder="Enter your Message" aria-required="true" aria-invalid="false"></textarea >
                                </div><!-- col-sm-6 -->
                                <div class="form-group col-sm-12">
                                    <button class="btn-submit" name="action" value="message" type="submit" id="form-submit">
                                        <i class="fa fa-paper-plane"></i>&nbsp;SEND MESSAGE</button>
                                </div>

                            </div><!-- row -->
                        </form>
                    </div><!-- contact-form-area -->
                </div><!-- col-sm-7 -->

            </div><!-- row -->
        </div><!-- container -->
    </section><!-- contact-area -->


@endsection