@extends('layouts.master')

@section('content')


    <div class="carousel-inner full-height">

        <div class="item active">
            <div class="layer black-layer"></div>
            <img src="images/contactus.jpg" alt="Main Slider">

            <div class="carousel-writing center-writing">
                <h1 class="carousel-heading title">LOGIN</h1>
                <!--					<h6 class="desc"><a href="#">HOME <i class="fa fa-angle-right"></i></a> CONTACT</h6>-->
            </div><!-- carousel-writing -->

        </div><!-- item -->

    </div><!-- carousel-inner -->
    </section><!-- carousel -->

    <section class="contact-area section" id="contact">
        <div class="container">
            <div class="row">



                <div class="col-sm-8">
                    <div class="contact-form-area">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif


                        <div class="col-md-8">
                        <h1>Sign In</h1>

                        <form method="post" action="{{ route('login') }}">
                            {{csrf_field()}}


                            <div class="form-group">
                                <label for="email">Email:</label>

                                <input type="email" class="form-control" id="email" name="email">
                            </div>

                            <div class="form-group">
                                <label for="password">Password:</label>

                                <input type="password" class="form-control" id="password" name="password">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Register</button>
                            </div>
                            @include('layouts.errors')

                        </form>
                        </div><!-- contact-form-area -->
                    </div><!-- col-sm-7 -->
                </div>

                </div><!-- row -->
            </div><!-- container -->
    </section><!-- contact-area -->


@endsection