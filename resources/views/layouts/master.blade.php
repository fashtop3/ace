<!DOCTYPE html>
<html lang="en">
<head>
    <title>Futa-Acm</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Goolge Font -->

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700%7CDroid+Sans:400,700" rel="stylesheet">

    <!-- Stylesheets -->

    <link href="/css/common-css/bootstrap.min.css" rel="stylesheet">



    <link href="/css/common-css/pe-icon-7-stroke.css" rel="stylesheet">
    <link href="/css/common-css/font-awesome.min.css" rel="stylesheet">

    <link href="/css/common-css/pe-icon-7-stroke.css" rel="stylesheet">


    <link href="/css/common-css/primary-styles-n-colors-2.css" rel="stylesheet">

    <link href="/css/common-css/page-animation.css" rel="stylesheet">

    <link href="/css/common-css/jquery-ui.css" rel="stylesheet">
    <link href="/css/common-css/jquery-ui.structure.css" rel="stylesheet">
    <link href="/css/common-css/jquery-ui.theme.css" rel="stylesheet">


    <link href="/css/layout-1/css/styles.css" rel="stylesheet">

    <link href="/css/layout-1/css/colors.css" rel="stylesheet">

    <link href="/css/layout-1/css/responsive.css" rel="stylesheet">

    <link href="/css/contact/css/styles.css" rel="stylesheet">

    <link href="/css/contact/css/colors.css" rel="stylesheet">

    <link href="/css/contact/css/responsive.css" rel="stylesheet">



</head>
<body>

<section id="main-carousel" class="carousel fade carousel-main" >
    <header id="header">

       @include('layouts.nav')
    </header>

    @yield('content')










@include('layouts.footer')
    <a href="#header" class="move-to-top scroll-animation-enable" ><i class="fa fa-angle-double-up"></i></a>

    <script src="/js/common-js/jquery-3.1.1.min.js"></script>
    <script src="/js/common-js/jquery-ui.min.js"></script>
    <script src="/js/common-js/owl.carousel.js"></script>
    <script src="/js/common-js/bootstrap.min.js"></script>
    <script src="/js/common-js/SmoothScroll.min.js"></script>
    <script src="/js/common-js/scripts.js"></script>

</body>
</html>