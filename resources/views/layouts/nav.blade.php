<nav class="main-menu navbar-default" id="main-menu" >
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="main-menu-inner">

                    <div class="navbar-header">
                        <a href="#" class="main-logo">
                            <!-- If the background changes then set data-img-change="enable"  -->
                            <img src="/images/acm.png" alt="Logo" data-img-change="enable">
                        </a>
                    </div><!-- navbar-header -->

                    <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>

                    <div class="collapse navbar-collapse" id="main-nav">
                        <ul class="nav navbar-nav main-nav">
                            <li class="active dropdown"><a href="/">HOME</a>

                            </li>
                            <li class="dropdown"><a href="#" data-toggle="dropdown">SPECIAL INTEREST GROUPS
                                    <i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#ms">Microsoft</a></li>
                                    <li><a href="#intel" >Google</a></li>
                                    <li><a href="#google" >Intel</a></li>

                                </ul>
                            </li>

                            <li class="dropdown"><a href="#" data-toggle="dropdown">MEMBERSHIP
                                    <i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu">
                                    <li><a href="/acmembers">MEMBERSHIP</a></li>
                                    <li><a href="/acmembers/create">JOIN FUTA-ACM</a></li>

                                </ul>
                            </li>
                            <li><a href="/contact">CONTACT</a></li>
                            <li><a href="/communities/create">JOIN A COMMUNITY</a></li>

                        </ul>
                    </div>

                </div><!-- navinner -->
            </div><!-- col-sm-12 -->
        </div><!-- row -->
    </div><!-- container -->
</nav><!-- main-menu navbar navbar-default -->
