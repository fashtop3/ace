<footer>

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="footer-bottom-inner">
                    <div class="col-sm-6 reset-padding">
                        <p class="copyright-writing">Copyright <i class="fa fa-copyright"></i>
                            2017 All Rights Reserved by Futa-Acm</p>
                    </div><!-- left-align -->
                    <div class="col-sm-6 reset-padding">
                        <ul class="icons ">
                            <li><a href=""><i class="fa fa-facebook"></i></a></li>
                            <li><a href=""><i class="fa fa-twitter"></i></a></li>
                            <li><a href=""><i class="fa fa-google"></i></a></li>

                        </ul>
                    </div><!-- col-sm-6 -->
                </div><!-- footer-bottm-inner -->
            </div><!-- row -->
        </div><!-- container -->
    </div><!-- footer-bottom -->
</footer>

