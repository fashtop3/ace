<!DOCTYPE html>
<html lang="en">
<head>
    <title>Futa-Acm</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Goolge Font -->

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700%7CDroid+Sans:400,700" rel="stylesheet">


    <!-- Stylesheets -->

    <link href="/css/common-css/bootstrap.min.css" rel="stylesheet">

    <link href="/css/common-css/font-awesome.min.css" rel="stylesheet">

    <link href="/css/common-css/pe-icon-7-stroke.css" rel="stylesheet">


    <link href="/css/common-css/primary-styles-n-colors-2.css" rel="stylesheet">

    <link href="/css/common-css/page-animation.css" rel="stylesheet">


    <link href="/css/blog-single/css/styles.css" rel="stylesheet">

    <link href="/css/blog-single/css/colors.css" rel="stylesheet">

    <link href="/css/blog-single/css/responsive.css" rel="stylesheet">


</head>
<body style="height: 100vh;">

<section id="osthir-carousel" class="carousel fade" >
    <header id="header">

        <nav class="main-menu navbar-default" id="main-menu" >
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="main-menu-inner">

                            <div class="navbar-header">
                                <a href="#" class="main-logo">
                                    <!-- If the background changes then set data-img-change="enable"  -->
                                    <img src="/images/acm.png" alt="Logo" data-img-change="enable">
                                </a>
                            </div><!-- navbar-header -->

                            <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </button>

                            <div class="collapse navbar-collapse" id="main-nav">
                                <ul class="nav navbar-nav main-nav">

                                    <li><a href="/news/news">NEWS</a></li>
                                    <li><a href="/events/events">EVENTS</a></li>
                                    <li><a href="/admin/acmembers">ACM MEMBERS</a></li>
                                    <li><a href="{{ route('communities') }}">COMMUNITIES</a></li>
                                    <li><a href="/admin/message">MESSAGES</a></li>
                                    <li><a href="/register">Register new Admin</a></li>
                                    @if(Auth::check())
                                    <li><a>{{Auth::user()->name}}</a></li>
                                        @endif
                                    <li><a href="/logout">Logout</a></li>
                                </ul>
                            </div>

                        </div><!-- navinner -->
                    </div><!-- col-sm-12 -->
                </div><!-- row -->
            </div><!-- container -->
        </nav><!-- main-menu navbar navbar-default -->


    </header>





    @yield('content')










    @include('layouts.adfooter')
    <a href="#header" class="move-to-top scroll-animation-enable" ><i class="fa fa-angle-double-up"></i></a>

    <script src="/js/common-js/jquery-3.1.1.min.js"></script>
    <script src="/js/common-js/owl.carousel.js"></script>
    <script src="/js/common-js/bootstrap.min.js"></script>
    <script src="/js/common-js/SmoothScroll.min.js"></script>
    <script src="/js/common-js/scripts.js"></script>

