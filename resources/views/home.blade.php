@extends('layouts.master')



@section('content')

    <a href="#main-carousel" class="slider-footer-btn carousel-control-prev" data-slide="prev">
        <i class="pe-7s-angle-left"></i></a>
    <a href="#main-carousel" class="slider-footer-btn carousel-control-next" data-slide="next">
        <i class="pe-7s-angle-rinoght"></i></a>
    <div class="carousel-inner full-height">

        <div class="item active">
            <div class="layer black-layer"></div>
            <img src="images/dbanner.jpg" alt="Main Slider">

            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="carousel-writing center-writing">

                            <h1 data-animation="animated slider-scale-down" class="carousel-heading animation-delay-2">
                                FUTA ASSOCIATION FOR COMPUTING MACHINERY
                            </h1>

                            <h4 data-animation="animated slider-scale-down" class="carousel-desc animation-delay-3 normal-text">
                                The world's largest educational and scientific computing society, delivers resources that advance computing as a science and a profession.
                            </h4>

                            <a data-animation="animated slider-scale-down" href="/acmembers/create"
                               class="carousel-btn apply-btn animation-delay-4"><strong>JOIN</strong></a>

                        </div><!-- carousel-writing -->
                    </div><!-- col-sm-6 -->
                </div><!-- row -->
            </div><!-- Container -->
        </div><!-- item -->

        <div class="item">
            <div class="layer black-layer"></div>
            <img src="images/futbanner.JPG" alt="Main Slider">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="carousel-writing center-writing">

                            <h1 data-animation="animated slider-scale-down" class="carousel-heading animation-delay-2">
                                FUTA ASSOCIATION FOR COMPUTING MACHINERY
                            </h1>

                            <h4 data-animation="animated slider-scale-down" class="carousel-desc animation-delay-3 normal-text">
                                The world's largest educational and scientific computing society, delivers resources that advance computing as a science and a profession.	</h4>

                            <a data-animation="animated slider-scale-down" href="join.php"
                               class="carousel-btn apply-btn animation-delay-4"><strong>JOIN</strong></a>

                        </div><!-- carousel-writing -->
                    </div><!-- col-sm-6 -->
                </div><!-- row -->
            </div><!-- Container -->
        </div><!-- item -->

    </div><!-- carousel-inner -->
    </section><!-- carousel -->

    <section class="services-area" id="services">
        <div class="container">
            <div class="row">

                <div class="col-sm-12">
                    <div class="heading center-text">
                        <h3>SPECIAL INTEREST GROUPS</h3>
                        <h6 class="heading-desc">ACM's Special Interest Groups (SIGs) represent major areas of computing,<br> addressing the interests of technical communities that drive innovation.</h6>

                        <br>
                    </div><!-- heading -->
                </div>

                <div class="col-sm-4">
                    <div class="service" id="ms">
                        <div class="service-content">
                            <i class="icon "><img src="images/ms.png" style="width: 50px;"></i>
                            <!--                            <img src="images/ms.png">-->
                            <h5 class="title">MICROSOFT COMMUNITY</h5>
                            <p class="desc">We are empowering every organization and individuals on the planet to achieve more</p>
                        </div><!-- service-content -->
                        <br>
                        <a href="/communities/create" class="o-btn">
                            <span class="btn_label"><strong>JOIN</strong></span>
                        </a>
                    </div><!-- service -->
                </div><!-- col-sm-4 -->
                <div class="col-sm-4">
                    <div class="service" id="intel">
                        <div class="service-content">
                            <i class="icon "><img src="images/intblu.png" style="width: 50px;"></i>
                            <h5 class="title">INTEL COMMUNITY</h5>
                            <p class="desc">We work relentlessly to deliver the platform and technology advancements that become essential to the way we work and live</p>
                        </div><!-- service-content -->
                        <a href="/communities/create" class="o-btn">
                            <span class="btn_label"><strong>JOIN</strong></span>
                        </a>
                    </div><!-- service -->
                </div><!-- col-sm-4 -->
                <div class="col-sm-4">
                    <div class="service" id="google">
                        <div class="service-content">
                            <i class="icon "><img src="images/google.png" style="width: 50px;"></i>

                            <h5 class="title">GOOGLE</h5>
                            <p class="desc">Let's organize the world's information and make it universally accessible and useful. </p>
                        </div><!-- service-content -->
                        <br>
                        <a href="/communities/create" class="o-btn">
                            <span class="btn_label"><strong>JOIN</strong></span>
                        </a>
                    </div><!-- service -->
                </div><!-- col-sm-4 -->

            </div><!-- row -->
        </div><!-- container -->
    </section><!-- services-area -->

    <section class="section featured-area enable-load-effect" id="featured">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                <div class="col-sm-offset-1 col-sm-10">

                    <div class="featured-writing">
                        <div class="heading">
                            <h6 class="heading-desc">DO EXTRA ORDINARY THINGS IN EXTRAORDINARY WAYS</h6>
                            <h3>WELCOME TO FUTA-ACM</h3>
                        </div><!-- heading -->
                    </div><!-- featured-writing -->
                </div><!-- col-sm-12 -->

                <div class="col-sm-6 ">
                    <div class="service-content first-service" data-load-effect="fade-from-left">
                        <div class="title">
                            <h5>VISION</h5>
                        </div><!-- title -->
                        <p class="desc">To be a cutting edge tech student community, vast in knowledge,
                            use of technologies( design, development , construction, management and applications of modern computing)
                            and reaching out to individuals with interest in technology  </p>

                        <br>
                        <br>

                    </div><!-- service-content -->

                    <br>
                    <br>

                </div><!-- col-sm-12 -->

                <div class="col-sm-4 col-sm-offset-1">
                    <div class="image-wrapper">
                        <img src="images/acm.png" alt="">
                    </div>
                </div><!-- col-sm-12 -->


            </div><!-- row -->
            </div>
        </div><!-- container -->
    </section><!-- featured-area -->





    <section class="section teachers-area" id="teachers">
        <div class="container">
            <div class="row">

                <div class="col-sm-12">
                    <div class="heading center-text">

                        <h3>FACULTY SPONSORS</h3>
                    </div><!-- heading -->
                </div>

                <div class="col-sm-6 col-md-5 col-md-offset-1">
                    <div class="teacher">
                        <div class="image-wrapper">
                            <a href="#"><img src="images/profA.jpg" alt="Profile Picture" ></a>
                        </div><!-- image-wrapper -->

                        <div class="title">
                            <h5>Prof. Boniface Alese</h5>

                        </div><!-- title -->


                    </div><!-- teacher -->
                </div><!-- col-sm-6 col-md-3 -->

                <div class="col-sm-6 col-md-6">
                    <div class="teacher">

                        <div class="image-wrapper">
                            <a href="#"><img src="images/Aderonke%20pix-1.jpg" alt="Profile Picture" ></a>
                        </div><!-- image-wrapper -->
                        <div class="title">
                            <h5>Dr (Mrs) Thompson</h5>

                        </div><!-- title -->



                    </div><!-- teacher -->
                </div><!-- col-sm-6 col-md-3 -->


            </div><!-- row -->
        </div><!-- container -->
    </section><!-- teachers-area -->



    <section class="section events-notice-area" id="events" style="padding-top: 0px;">
        <div class="container">
            <div class="row">

                <div class="col-sm-12">
                    <div class="heading margin-left-3 center-text">
                        <h3>UPCOMING EVENTS</h3>
                    </div><!-- heading -->
                </div>


                @foreach($events as $event)
                <div class="col-sm-6 col-md-4">
                    <div class="event">
                        <div class="date">
                            <h1 class="main-date"><b>{{$event->date}}</b></h1> <h4 class="month">{{$event->month}} {{$event->date}}</h4>
                        </div>
                        <div class="desc-wrapper">
                            <h5 class="title"><a href="#">{{$event->title}}</a></h5>
                            <p class="location">
                                <span><i class="fa fa-clock-o"></i>{{$event->duration}}</span>
                                <span><i class="fa fa-map-marker"></i>{{$event->location}}</span></p>
                            <p class="desc">{{$event->body}}</p>
                        </div>
                    </div><!-- event -->
                </div><!-- col-sm-6 -->
                @endforeach



                </div><!-- row -->
            </div><!-- container -->
    </section><!-- event -->



    <!-- quoto-area -->


    <section class="latest-news-area section enable-load-effect" id="latest-news">
        <div class="container">
            <div class="row">

                <div class="col-sm-12">
                    <div class="heading center-text">
                        <h3>LATEST NEWS</h3>
                    </div><!-- heading -->
                </div><!-- col-sm-12 -->


                @foreach($news as $new)
                <div class="col-sm-6 col-md-4">
                    <div class="news" data-load-effect="fade-from-bottom">

                        <div class="image-wrapper">
                            <img src="/storage/{{$new->picture}}" alt="Profile Picture" >
                        </div><!-- image-wrapper -->

                        <div class="desc-wrapper">
                            <h5 class="title"><a href="#">{{ $new->title }}</a></h5>
                            <p class="desc">
                                {{ $new->body }}
                            </p>
                            <h6 class="read-more-btn color-with-icon-hover"><a href="{{ $new->url }}">READ MORE
                                    <i class="fa fa-long-arrow-right"></i></a></h6>
                        </div><!-- desc-wrapper -->

                    </div><!-- news -->
                </div><!-- col-sm-4 -->
                @endforeach

            </div><!-- row -->
        </div><!-- container -->
    </section><!-- latest-news -->




@endsection



