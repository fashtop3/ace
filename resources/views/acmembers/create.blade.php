@extends('layouts.master')

@section('content')

    <div class="carousel-inner full-height">

        <div class="item active" style="position: relative;
    top: 35px;">
            <div class="layer black-layer"></div>
            <img src="/images/mem.PNG" alt="Main Slider" >
            <!---->
            <div class="carousel-writing center-writing">
                <h1 class="carousel-heading title">JOIN US</h1>
                <!--<h6 class="desc"><a href="#">HOME <i class="fa fa-angle-right"></i></a> ABOUT US</h6>-->
            </div>

        </div><!-- item -->

    </div><!-- carousel-inner -->
    </section><!-- carousel -->

    <section class="contact-area section" id="contact">
        <div class="container">
            <div class="row">

                <div class="col-sm-12">
                    <div class="heading">
                        <h3></h3>WELCOME</h3>
                        <h6 class="heading-desc">WE are pleased to have you join us</h6>
                    </div><!-- heading -->
                </div><!-- col-sm-12 -->



                <div class="col-sm-6 col-sm-offset-3">
                    <div class="contact-form-area">

                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                            @include('layouts.errors')
                        <form method="post" action="/acmembers" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="row">


                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="FullName" id="name" name="name" >
                                </div>


                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Email" id="email" name="email" >
                                    </div>



                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Level" id="level" name="level" >
                                    </div>



                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Department" id="dept" name="dept" >
                                    </div>


                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Phone Number" id="phone" name="phone" >
                                    </div>



                                <div class="form-group">
                                    <input type="file" name="picture" id="picture">
                                </div>

                                <div class="form-group col-sm-12">
                                    <button class="btn-submit" name="action" value="join" type="submit" id="form-join">
                                         <i class="fa fa-paper-plane"></i>Become A Member</button>
                                </div>
                            </div>


                        </form>



                    </div><!-- contact-form-area -->
                </div><!-- col-sm-7 -->

            </div><!-- row -->
        </div><!-- container -->
    </section><!-- contact-area -->



@endsection