@extends('layouts.master')

@section('content')

    <div class="carousel-inner full-height">

        <div class="item active" style="position: relative;
    top: 35px;">
            <div class="layer black-layer"></div>
            <img src="/images/mem.PNG" alt="Main Slider" >
            <!---->
            <div class="carousel-writing center-writing">
                <h1 class="carousel-heading title">MEMBERS</h1>
                <!--<h6 class="desc"><a href="#">HOME <i class="fa fa-angle-right"></i></a> ABOUT US</h6>-->
            </div>

        </div><!-- item -->

    </div><!-- carousel-inner -->
    </section><!-- carousel -->



    <section class="section teachers-area" id="teachers">
        <div class="container">
            <div class="row">

                <div class="col-sm-12">
                    <div class="heading center-text">
                        <h6 class="heading-desc">THIS IS FUTA-ACM</h6>
                        <h3>CHAPTER MEMBERS AND EXECUTIVES</h3>
                    </div><!-- heading -->
                </div>

                @foreach($acmembers as $acmember )
                <div class="col-sm-6 col-md-2 col-md-offset-1">
                    <div class="teacher">
                        <div class="image-wrapper">
                            <a href="#"><img src="/storage/{{($acmember->picture)}}" alt="Profile Picture" style="height: 160px;" ></a>
                        </div><!-- image-wrapper -->
                        <div class="title">
                            <h5>{{$acmember->name}}</h5>

                        </div><!-- title -->

                    </div><!-- teacher -->
                </div><!-- col-sm-6 col-md-3 -->
                @endforeach

            </div><!-- row -->
        </div><!-- container -->
    </section><!-- teachers-area -->

@endsection