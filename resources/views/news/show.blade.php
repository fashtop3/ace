@extends('layouts.admaster')

@section('content')


    <section class="blog-area" id="featured">
        <div class="container">
            <div class="row">

                <div class="col-sm-12 col-md-9">

                    <div class="blog-post">


                        <div class="leave-reply-area">
                            <h4 class="title">Leave a reply</h4>
                            <form method="post" action="/news/show" enctype="multipart/form-data">
                                {{csrf_field()}}

                                <div class="form-group">
                                    <div class="form-group">

                                        <input type="text" placeholder=" Title" class="form-control" id="title" name="title" >
                                    </div>

                                    <div class="form-group">

                                        <textarea id="body" name="body" placeholder="Body of news" maxlength="254" class="form-control"></textarea>
                                    </div>
                                    <div class="form-group">

                                        <input type="text" placeholder=" Enter news url" class="form-control" maxlength="254" id="url" name="url" >
                                    </div>


                                    <div class="form-group">
                                        <input type="file" name="picture" id="picture">
                                    </div>

                                    <div class="form-group col-sm-12" >
                                        <button class="btn-submit" type="submit" id="form-submit"> SUBMIT NEWS</button>
                                    </div><!-- form-group -->
                                </div>
                            </form>
                        </div><!-- leave-reply-area -->

                    </div><!-- blog-post-->
                </div><!-- col-md-8 -->

                <div class="col-md-3">

                    <div class="popular-posts">
                        <h4 class="title">All news</h4>
                        @foreach($news as $new )

                        <div class="popular-post">
                            <a href="#" class="post-image"><img src="/storage/{{($new->picture)}}" alt="Recent Post Image" style="width: 40px;"></a>
                            <div class="desc">
                                <h6 class="post-name"><a href="#">{{$new->title}}</a></h6>
                            </div>

                            <form action="{{ route('del', [$new->id]) }}" method="post">
                                {{ csrf_field() }}

                                <button class="btn btn-sm btn-danger btn-submit" type="submit" id="form-submit"> Delete</button>
                            </form>
                        </div><!-- popular-post -->



                        @endforeach

                    </div><!-- popular-posts -->


                </div><!-- col-md-3 -->

            </div><!-- row -->
        </div><!-- container -->
    </section><!-- blog-area -->





{{--@foreach($news as $new )--}}
    {{--<li> {{$new->title}}</li>--}}
    {{--<li> {{$new->body}}</li>--}}

    {{--<img src="{{asset($new->picture)}}" style="width:50px; height: 50px">--}}

{{--@endforeach--}}

@endsection

