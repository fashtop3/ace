<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Model extends Eloquent
{
    protected $fillable = ['title', 'body','picture', 'duration','location',
        'date', 'month','name','email', 'level', 'dept', 'phone','message','subject','department','sig','url'];
}
