<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['home']);
    }


    public function index()
    {
        return view('news.index');
    }

    public function create()
    {
        return view('news.create');
    }

    public function store()
    {
        $this->validate(request(),[
            'title' => 'required',
            'body' => 'required|max:254',
            'url' => 'required|max:254',
            'picture' => 'required|image|mimes:jpeg,png,jpg'

        ]);


        News::create([
            'title' => request('title'),
            'body'=>request('body'),
            'url' => request('url'),
            'picture' => request()->file('picture')->store('images')
        ]);

        return redirect('/news/show');
    }

    public function show()
    {
        $news = News::all();
        return view('news.show',compact('news'));

    }

    public function home()
    {
        $news = News::orderBy('id', 'desc')->take(3)->get();
        return view('home',compact('news'));
    }

    public function destroy($id)
    {
        $news = News::find($id)->delete();

        return redirect('/news/show');
    }

}
