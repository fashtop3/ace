<?php

namespace App\Http\Controllers;

use App\Acmember;
use Illuminate\Http\Exceptions\PostTooLargeException;
use Illuminate\Http\Request;

class AcmembersController extends Controller
{
    public function create()
    {
        return view('acmembers.create'); //acm join page
    }
    public function store()
    {

        $this->validate(request(),[
            'name' => 'required',
            'email' => 'required|unique:acmembers',
            'level' => 'required',
            'dept' => 'required',
            'phone' => 'required',
            'picture' => 'required|image|mimes:jpeg,png,jpg'
        ]);

//        $picture = 'picture' => request()->file('picture')->store('image');

        try{

          Acmember::create([
            'name' => request('name'),
            'email'=>request('email'),
            'level'=>request('level'),
            'dept'=>request('dept'),
            'phone'=>request('phone'),
            'picture' => request()->file('picture')->store('image')
        ]);



        }

        catch(PostTooLargeException $e){

        }
        return back()->with('status', 'Form submitted');

    }
    public function show()
    {
        $acmembers  = Acmember::all();
        return view('acmembers.show', compact('acmembers'));

    }
    public function admin(){
        $acmembers  = Acmember::all();
        return view('admin.acmembers', compact('acmembers'));

    }

    public function search(Request $request)
    {

        $acmembers  = Acmember::where(function ($query) use($request) {
            $s = explode(' ', $request->get('q'));
            foreach ($s as $value) {
                $query->orWhere('department', "%{$value}%");
                $query->orWhere('fullname', "%{$value}%");
                $query->orWhere('store', "%{$value}%");
            }
        })->get();

        return view('admin.acmembers', compact('acmembers'));
    }
}
