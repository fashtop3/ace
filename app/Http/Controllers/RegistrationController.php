<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegistrationRequest;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class RegistrationController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
    }

    public function create()
    {
        return view('registration.create');

    }
    public function store(RegistrationRequest $request)
    {
        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => \Hash::make($request->get('password'))
        ]);


        auth()->login($user);

        return view('admin.home');
    }


}
