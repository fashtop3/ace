<?php

namespace App\Http\Controllers;

use App\Community;
use Illuminate\Http\Request;

class CommunitiesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['create','store' ]);
    }

    public function create()
    {
      return view('communities.create');
    }
    public function store()
    {
        $this->validate(request(),[
            'name' => 'required',
            'email' => 'required|unique:communities',
            'level' => 'required',
            'dept' => 'required',
            'phone' => 'required',
            'sig' => 'required'
        ]);

        Community::create([
            'name' => request('name'),
            'email'=>request('email'),
            'level'=>request('level'),
            'dept'=>request('dept'),
            'phone'=>request('phone'),
            'sig' => request('sig')
        ]);

        return back()->with('status', 'Thanks for joining');


    }
    public function index(Request $request)
    {
        $communities = Community::all();

        $community_search  = Community::where(function ($query) use($request) {
            $text_input = explode(' ', $request -> get('search_field'));
            foreach ($text_input as $value) {
                $query->orWhere('dept', "%{$value}%");
                $query->orWhere('sig', "%{$value}%");
                $query->orWhere('level', "%{$value}%");
            }
        })->get();

        return view('communities.show', compact('communities','community_search'));
    }

    public function search(Request $request)
    {

        $community_search  = Community::where(function ($query) use($request) {
            $text_input = explode(' ', $request -> get('search_field'));
            foreach ($text_input as $value) {
                $query->orWhere('department', "%{$value}%");
                $query->orWhere('sig', "%{$value}%");
                $query->orWhere('level', "%{$value}%");
            }
        })->get();

        return view('communities.show', compact('communities'));
    }
}
