<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Authenticatable;
//use Illuminate\Foundation\Auth\AuthenticatesUsers;

class SessionsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        return view('sessions.create');
    }

    public function store()
    {
        //this is where we attempt to authenticate the user;

        if( Auth:: attempt(request(['email','password'])))
        {
            return back()->with('status', 'please check your login credentials and try again');
        }
        else{



        $user = Auth::user();
            return view('admin.home', compact('user'));
        }


    }

    public function destroy()
    {
        auth()->logout();
        return redirect('/');


    }
}
