<?php

namespace App\Http\Controllers;

use App\Event;
use App\News;
use Illuminate\Http\Request;

class EventsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['store', 'home']);
    }

    public function store()
    {
        $this->validate(request(),[
            'title' => 'required|max:150',
            'body' => 'required|max:254',
            'duration' => 'required',
            'location' => 'required',
            'date' => 'required',
            'month' =>'required'
        ]);

        Event::create([
            'title' => request('title'),
            'body' => request('body'),
            'duration' => request('duration'),
            'location' => request('location'),
            'date' => request('date'),
            'month'=> request('month')

        ]);
        return redirect('/events/show');
    }

    public function show()
    {
        $events = Event::all();
        return view('events.show', compact('events'));
    }

    public function home()
    {
        $events = Event::orderBy('id', 'desc')->take(3)->get();
        $news = News::orderBy('id', 'desc')->take(3)->get();
        return view('home', compact('events','news'));
    }

    public function destroy($id)
    {
        $events = Event::find($id)->delete();

        return redirect('/events/show');
    }

}
