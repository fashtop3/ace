<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function create()
    {
        return view('contact');
    }
    public function store()
    {
        $this->validate(request(),[
            'name' => 'required',
            'email' => 'required',
            'department' => 'required',
            'subject' => 'required|max:254',
            'message' => 'required|max:254'
            ]);

        Contact::create([
            'name' => request('name'),
            'email' => request('email'),
            'department' => request('department'),
            'subject' => request('subject'),
            'message' => request('message')
        ]);

        return back()->with('status', 'Your messsage has been delivered, we would stay in touch');
    }

    public function show()
    {
         $messages  = Contact::all();
        return view('admin.message', compact('messages'));

    }
}
